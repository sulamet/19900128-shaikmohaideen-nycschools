//
//  NetworkHelper.swift
//  NetworkManager
//
//  Created by Shaik Mohaideen A S on 3/14/23..
//

import Foundation

/// Helper class to get data from RequestManager and Response Manager 
public class NetworkHelper {
    
    public class func fetchResponse <T: Codable>(url : String, responseCalss : T.Type,  completion: @escaping (Bool, T?) -> ()) {
        RequestManager.shared.makeUrlRequest(url: url) { success, responseData in
            if success, let respose = responseData {
                ResponseManager.responseMapper(data: respose, responseClass: responseCalss) { success, responseObject in
                    if success {
                        return completion(success, responseObject)
                    }else{
                        completion(success, nil)
                    }
                }
            }
        }
    }
}


