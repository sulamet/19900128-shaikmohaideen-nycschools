//
//  RequestManager.swift
//  NetworkManager
//
//  Created by Shaik Mohaideen A S on 3/14/23..
//

import Foundation
import Security

/// Request Manager is only responsible for calling setvice and get data SOLID - Followed Single Principle
class RequestManager: NSObject, URLSessionDelegate {
    static let shared = RequestManager()
    
    func makeUrlRequest(url : String,  completion: @escaping (Bool, Data?) -> ()) {
        
        let session = URLSession.init(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)
        
        guard let url = URL(string: url) else {
            return completion(false, nil)
        }
        
        var uRLRequest = URLRequest (url: url)
        uRLRequest.httpMethod = "GET"
        
        session.dataTask(with: uRLRequest) { data, response, error in
            
            if let responseData = data {
                return completion(true, responseData)
            }else {
                return completion(false, nil)
            }
        }.resume()
        
    }
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void)    {
        let protectionSpace = challenge.protectionSpace
        
        guard protectionSpace.authenticationMethod ==            NSURLAuthenticationMethodServerTrust,            protectionSpace.host.contains("data.cityofnewyork.us") else
        {
            return completionHandler(.performDefaultHandling, nil)
        }
        
        guard let serverTrust = protectionSpace.serverTrust else { completionHandler(.performDefaultHandling, nil)
            return
            
        }
        let credential = URLCredential(trust: serverTrust)
        completionHandler(.useCredential, credential)
    }
    
    
}


