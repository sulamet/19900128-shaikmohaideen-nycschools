//
//  ViewController.swift
//  NYC_APP
//
//  Created by Shaik Mohaideen A S on 3/14/23.
//

import UIKit


class ViewController: UITableViewController {
    var schoolListArray = [NYCSchoolListModel]()
    var schoolDetailArray = [NYCSchoolDetailsModel]()
    let activityView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
    let schoolListCellIdentifier = "SchoolListTableviewCell"
    let group = DispatchGroup()
    let schoolDetailVM = SchoolDetailViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        showActivityIndicatory()
        fetchServiceCall()
        fetchDetailService()
        // Dispatch group to maintain multiple service call
        group.notify(queue: .main) {
            self.tableView.reloadData()
            self.activityView.stopAnimating()
        }
    }
    
    /// Fetch List
    func fetchServiceCall () {
        group.enter()
        NetworkHelper.fetchResponse(url: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json", responseCalss: [NYCSchoolListModel].self) { [weak self] success, reaponseObj in
            if success, let response = reaponseObj {
                DispatchQueue.main.async {
                    self?.schoolListArray = response
                    self?.group.leave()
                }
            }
        }
    }
    
    /// Fetch Detail to load data seemlessly
    func fetchDetailService () {
        group.enter()
        NetworkHelper.fetchResponse(url: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json", responseCalss: [NYCSchoolDetailsModel].self) { [weak self] success, reaponseObj in
            if success, let response = reaponseObj {
                DispatchQueue.main.async {
                    self?.schoolDetailArray = response
                    self?.group.leave()
                }
            }
        }
    }
    
    func showActivityIndicatory() {
        activityView.center = self.view.center
        activityView.hidesWhenStopped = true
        self.view.addSubview(activityView)
        activityView.startAnimating()
    }
    
    
}

// Table view Datasource
extension ViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolListArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: schoolListCellIdentifier, for: indexPath) as? SchoolListTableviewCell
        
        cell?.subTitleLabel?.text = schoolListArray[indexPath.row].schoolName
        
        return cell ?? UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
// Table view Delegate
extension ViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let filterModel = schoolDetailVM.getDetailModelwithId(schoolListArray[indexPath.row].dbn, detailList: schoolDetailArray), filterModel.count > 0, let firstObject = filterModel.first
        {
            let detailModel = schoolDetailVM.mapModelFromResponse(schoolListArray[indexPath.row], firstObject)
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
            nextViewController.schoolDetailViewModel = detailModel
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }else {
            
            // create the alert
            let alert = UIAlertController(title: "No Data!", message: "No record found for this school", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
}
